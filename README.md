### Sunum ###

Sunum 'sunum' klasoru altinda 'takim-3-sunum.m4v' dosyasidir.

##

### Uygulamanin calistirilmasi ###

BukalemunApplicationServer sinifi uzerinden calistirilmaktadir. Java 11 ve gradle 6.8.2 versiyonlari kullanilmistir.

'resource/bukalemun/build' paketi altinda 'classes' icerisinde manipule edilecek olan .class uzantili dosyalar
bulunmaktadir.

Projenin calismasi sonucunda 'customize_rules' altinda bulunan kurallarla, 'customize_classes' altindaki .class
siniflari olusmaktadir.
