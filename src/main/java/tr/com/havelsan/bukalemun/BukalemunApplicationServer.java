package tr.com.havelsan.bukalemun;

import com.fasterxml.jackson.databind.ObjectMapper;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.FieldInfo;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.IntegerMemberValue;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tr.com.havelsan.bukalemun.configuration.BukalemunApplicationConfiguration;
import tr.com.havelsan.bukalemun.model.CustomizeObject;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

@SpringBootApplication(scanBasePackageClasses = {BukalemunApplicationConfiguration.class})
public class BukalemunApplicationServer {

    public static final Logger LOGGER = LoggerFactory.getLogger(BukalemunApplicationServer.class);

    public static final String BASE_PATH = System.getProperty("user.dir") + "/resource/bukalemun/build";

    public static final String CUSTOMIZE_PATH = BASE_PATH + "/customize_rules/";

    public static final String BUILD_PATH = BASE_PATH + "/classes/";

    public static final String BUILD_CUSTOMIZE_PATH = BASE_PATH + "/customize_classes";

    public static final String JSON_EXTENSION = "json";

    public static void main(String[] args) {
        SpringApplication.run(BukalemunApplicationServer.class, args);
        prepareCustomizeClass(getCustomizeRule());
    }

    private static void prepareCustomizeClass(Map<String, CustomizeObject> customizeRuleMap) {
        if (MapUtils.isNotEmpty(customizeRuleMap)) {
            try (Stream<Path> walk = Files.walk(Path.of(BUILD_PATH))) {
                walk.filter(Files::isReadable)
                        .filter(Files::isRegularFile)
                        .forEach(path -> {
                            final var file = path.toFile();
                            final String filename = file.getName();
                            try (final var dataInputStream = new DataInputStream(new FileInputStream(file))) {
                                final var classFile = new ClassFile(dataInputStream);
                                final var constPool = classFile.getConstPool();
                                final var classFileName = classFile.getName();
                                final var customizeObject = customizeRuleMap.get(classFileName);
                                if (Objects.nonNull(customizeObject)) {
                                    if (LOGGER.isInfoEnabled()) {
                                        LOGGER.info(String.format("%s ozellestiriliyor...", classFileName));
                                    }
                                    final var feature = customizeObject.getFeature();
                                    if (Objects.nonNull(feature)) {
                                        final var constraintMap = feature.getConstraint();
                                        final List<FieldInfo> fieldInfoList = classFile.getFields();
                                        if (CollectionUtils.isNotEmpty(fieldInfoList)) {
                                            for (FieldInfo fieldInfo : fieldInfoList) {
                                                final var fieldName = fieldInfo.getName();
                                                final var constraint = constraintMap.get(fieldName);
                                                if (Objects.nonNull(constraint)) {
                                                    final Map<String, tr.com.havelsan.bukalemun.model.Annotation> annotationMap = constraint.getAnnotations();
                                                    if (MapUtils.isNotEmpty(annotationMap)) {
                                                        final var annotationsAttribute =
                                                                new AnnotationsAttribute(constPool, AnnotationsAttribute.visibleTag);
                                                        for (Map.Entry<String, tr.com.havelsan.bukalemun.model.Annotation> nameAndArgumentEntrySet
                                                                : annotationMap.entrySet()) {
                                                            annotationsAttribute.addAnnotation(getAnnotation(nameAndArgumentEntrySet, constPool));
                                                            fieldInfo.addAttribute(annotationsAttribute);
                                                        }
                                                    }
                                                } else {
                                                    if (LOGGER.isErrorEnabled()) {
                                                        LOGGER.error(String.format("customizeRuleMap icerisinde %s bulunmamaktadir.", fieldName));
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if (LOGGER.isErrorEnabled()) {
                                            LOGGER.error(String.format("%s json bilgisi icerisinde feature yer almamaktir.", classFileName));
                                        }
                                    }
                                } else {
                                    if (LOGGER.isErrorEnabled()) {
                                        LOGGER.error(String.format("%s json icerisinde yer almamaktir.", classFileName));
                                    }
                                }
                                dataInputStream.close();
                                final File customizeClassesFolder = new File(BUILD_CUSTOMIZE_PATH + "/" + System.currentTimeMillis());
                                customizeClassesFolder.mkdir();
                                try (var dataOutputStream
                                             = new DataOutputStream(
                                        new FileOutputStream(customizeClassesFolder.getPath() + "/" + filename)
                                )) {
                                    classFile.write(dataOutputStream);
                                }
                            } catch (IOException e) {
                                if (LOGGER.isErrorEnabled()) {
                                    LOGGER.error(String.format("%s okunurken hata olustu.", filename));
                                }
                            }
                        });
            } catch (IOException e) {
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error(String.format("%s okunurken hata olustu.", BUILD_PATH));
                }
            }
        } else {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("customizeRuleMap verisi bulunmamaktadir.");
            }
        }
    }

    private static Map<String, CustomizeObject> getCustomizeRule() {
        final Map<String, CustomizeObject> customizeObjectMap = new HashMap<>();
        try (Stream<Path> walk = Files.walk(Path.of(CUSTOMIZE_PATH))) {
            walk
                    .filter(Files::isReadable)
                    .filter(Files::isRegularFile)
                    .forEach(path -> {
                        final var fileName = path.getFileName().toString();
                        if (FilenameUtils.isExtension(fileName, JSON_EXTENSION)) {
                            try {
                                final var customizeObject
                                        = new ObjectMapper().readValue(path.toFile(), CustomizeObject.class);
                                final var identifier = customizeObject.getIdentifier();
                                if (customizeObjectMap.containsKey(identifier)) {
                                    if (LOGGER.isErrorEnabled()) {
                                        LOGGER.error(String.format("%s ait json bulunmaktadir...", identifier));
                                    }
                                } else {
                                    customizeObjectMap.put(identifier, customizeObject);
                                }
                            } catch (IOException e) {
                                if (LOGGER.isErrorEnabled()) {
                                    LOGGER.error(fileName + " okunurken hata olustu: ", e);
                                }
                            }
                        }
                    });
        } catch (IOException e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("%s okunurken hata olustu: ", CUSTOMIZE_PATH), e);
            }
        }
        return customizeObjectMap;
    }

    private static Annotation getAnnotation(Map.Entry<String, tr.com.havelsan.bukalemun.model.Annotation> nameAndArgumentEntrySet,
                                            javassist.bytecode.ConstPool constPool) {
        final String name = nameAndArgumentEntrySet.getKey();
        final Map<String, Object> arguments = nameAndArgumentEntrySet.getValue().getArguments();
        switch (name) {
            case "NotNull":
                return new Annotation(NotNull.class.getTypeName(), constPool);
            case "NotBlank":
                return new Annotation(NotBlank.class.getTypeName(), constPool);
            case "Size":
                final var annotation = new Annotation(Size.class.getTypeName(), constPool);
                if (MapUtils.isNotEmpty(arguments)) {
                    final var minMemberName = "min";
                    final Object minValue = arguments.get(minMemberName);
                    if (Objects.nonNull(minValue)) {
                        annotation.addMemberValue(minMemberName, new IntegerMemberValue(constPool, (Integer) minValue));
                    }
                    final var maxMemberName = "max";
                    final Object maxValue = arguments.get(maxMemberName);
                    if (Objects.nonNull(maxValue)) {
                        annotation.addMemberValue(maxMemberName, new IntegerMemberValue(constPool, (Integer) maxValue));
                    }
                }
                return annotation;
            default:
                throw new IllegalArgumentException(name + " annotation karsiligi bulunmamaktadir.");
        }
    }

}
