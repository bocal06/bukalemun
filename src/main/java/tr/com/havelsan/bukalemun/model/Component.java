package tr.com.havelsan.bukalemun.model;

public class Component {

    public boolean visible = true;

    public Component() {
    }

    public Component(boolean visible) {
        this.visible = visible;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

}
