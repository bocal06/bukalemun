package tr.com.havelsan.bukalemun.model;

import java.util.Map;

public class Feature {

    public Map<String, Constraint> constraint;

    public Feature() {
    }

    public Feature(Map<String, Constraint> constraint) {
        this.constraint = constraint;
    }

    public Map<String, Constraint> getConstraint() {
        return constraint;
    }

    public void setConstraint(Map<String, Constraint> constraint) {
        this.constraint = constraint;
    }

}