package tr.com.havelsan.bukalemun.model;

public class CustomizeObject {

    private String identifier;

    private Feature feature;

    public CustomizeObject() {
    }

    public CustomizeObject(String identifier, Feature feature) {
        this.identifier = identifier;
        this.feature = feature;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }
}
