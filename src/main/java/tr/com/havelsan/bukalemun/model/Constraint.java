package tr.com.havelsan.bukalemun.model;

import java.util.Map;

public class Constraint {

    public String name;

    public Map<String, Annotation> annotations;

    public Component component;

    public Constraint() {
    }

    public Constraint(String name, Map<String, Annotation> annotations, Component component) {
        this.name = name;
        this.annotations = annotations;
        this.component = component;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Annotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(Map<String, Annotation> annotations) {
        this.annotations = annotations;
    }

    public Component getComponent() {
        return component;
    }

    public void setComponent(Component component) {
        this.component = component;
    }
}