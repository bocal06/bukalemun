package tr.com.havelsan.bukalemun.model;

import java.util.Map;

public class Annotation {

    public Map<String, Object> arguments;

    public Annotation() {
    }

    public Annotation(Map<String, Object> arguments) {
        this.arguments = arguments;
    }

    public Map<String, Object> getArguments() {
        return arguments;
    }

    public void setArguments(Map<String, Object> arguments) {
        this.arguments = arguments;
    }
}