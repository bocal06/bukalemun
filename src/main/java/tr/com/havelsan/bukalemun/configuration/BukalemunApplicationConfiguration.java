package tr.com.havelsan.bukalemun.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import tr.com.havelsan.bukalemun.configuration.constant.BukalemunApplicationConfigurationConstant;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = {BukalemunApplicationConfigurationConstant.BASE_PACKAGE})
public class BukalemunApplicationConfiguration {
}
