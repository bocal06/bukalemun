package tr.com.havelsan.bukalemun.configuration.constant;

public final class BukalemunApplicationConfigurationConstant {

    private BukalemunApplicationConfigurationConstant() {
    }

    public static final String BASE_PACKAGE = "tr.com.havelsan.bukalemun";

}
